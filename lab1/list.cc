#include <iostream>
#include "list.h"

List::List() {
	first = nullptr;
}

List::~List() {
	if(first == nullptr) {
		return;
	}
	Node *loop = first;
	Node *loopNext = loop->next;
	delete loop;
	while(loopNext != nullptr){
		Node *loop = loopNext;
		loopNext = loopNext->next; 
		delete loop;
	}
	delete loopNext;
	return;
}

bool List::exists(int d) const {
	if(first == nullptr) {
		return false;
	}
	Node *loop = first;
	Node *loopNext = loop->next;
	if(loop->value == d) {
		return true;
	}
	while(loopNext != nullptr) {
		if(loopNext->value == d)  {
			return true;
		} else {
			loop = loopNext;
			loopNext = loopNext->next;
		}
	}
	return false;
}

int List::size() const {
	int size = 0;
	if(first == nullptr) {
		return size;
	}
	Node *loop = first;
	Node *loopNext = loop->next;
	size++;
	while(loopNext != nullptr) {
		size++;
		loopNext = loopNext->next;
	}
	return size;
}

bool List::empty() const {
	return first == nullptr;
}

void List::insertFirst(int d) {
	Node *temp = new Node{d, first};
	first = temp;
	return;
}

void List::remove(int d, DeleteFlag df) {
	if(first == nullptr) {
		return;
	}
	else if((df == DeleteFlag::EQUAL && first->value == d) || (df == DeleteFlag::LESS && first->value < d) || (df == DeleteFlag::GREATER && first->value > d)) {
		first = first->next;
		return;
	}
	Node *current = first;
	Node *next = first->next;
	while(next != nullptr) {
		if((df == DeleteFlag::EQUAL && next->value == d) || (df == DeleteFlag::LESS && next->value < d) || (df == DeleteFlag::GREATER && next->value > d)) {
			current->next = next->next;
			delete next;

			return;
		}
		else {
			current = next;
			next = next->next;
		}
	}
}

void List::print() const {
	if(first == nullptr) {
		std::cout << "\n";
		return;
	}
	Node *current = first;
	while(current != nullptr) {
		std::cout << current->value << '\n';
		current = current->next;
	}
	return;
}

