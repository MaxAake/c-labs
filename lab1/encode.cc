#include <iostream>
#include <fstream>
#include <string>
#include "coding.h"


int main() {
    std::string filename;
    std::string encoded;
    unsigned char character;

    std::cout << "enter filename\n";
    std::cin >> filename;
    std::cout << filename << "\n";
    std::ifstream inData;
    inData.open(filename);
    std::ofstream enc("encoded.enc");
    while(inData.peek() != EOF) {
        character = inData.get();
        character = encode(character);
        std::cout << character << "\n";
        enc.put(character); 
    }

    enc.close();
    inData.close();
    return 0;
}